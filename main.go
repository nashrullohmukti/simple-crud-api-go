package main

import (
	"gitlab.com/nashrullohmukti/simple-crud-api-go/app"
)

func main() {
	app.Routes()
}
