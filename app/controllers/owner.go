package controllers

import (
	"net/http"

	"github.com/gin-gonic/gin"
	"gitlab.com/nashrullohmukti/simple-crud-api-go/app/models"
)

func FindOwners(c *gin.Context) {
	var owners []models.Owner
	models.DB.Find(&owners)

	c.JSON(http.StatusOK, gin.H{"data": owners})
}

func CreateOwner(c *gin.Context) {
	var input models.CreateOwnerInput
	if err := c.ShouldBindJSON(&input); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	owner := models.Owner{Name: input.Name, Email: input.Email, PhoneNumber: input.PhoneNumber, NpwpNumber: input.NpwpNumber}
	models.DB.Create(&owner)

	c.JSON(http.StatusOK, gin.H{"data": owner})
}

func FindOwner(c *gin.Context) {
	var owner models.Owner

	if err := models.DB.Where("id = ?", c.Param("id")).First(&owner).Error; err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Record not found!"})
		return
	}

	c.JSON(http.StatusOK, gin.H{"data": owner})
}

func UpdateOwner(c *gin.Context) {
	var owner models.Owner

	if err := models.DB.Where("id = ?", c.Param("id")).First(&owner).Error; err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Record not found!"})
		return
	}

	var input models.UpdateOwnerInput
	if err := c.ShouldBindJSON(&input); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	models.DB.Model(&owner).Updates(input)

	c.JSON(http.StatusOK, gin.H{"data": owner})
}

func DeleteOwner(c *gin.Context) {
	var owner models.Owner

	if err := models.DB.Where("id = ?", c.Param("id")).First(&owner).Error; err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Record not found!"})
		return
	}

	models.DB.Delete(&owner)

	c.JSON(http.StatusOK, gin.H{"data": true})
}
