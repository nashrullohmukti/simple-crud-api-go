package models

import (
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/postgres"
)

var DB *gorm.DB

const (
	DBDRIVER = "postgres"
	DBUSER   = "postgres"
	DBPASS   = "postgres"
	DBNAME   = "partner_service_development"
	DBHOST   = "localhost"
	DBPORT   = "5432"
)

func ConnectDatabase() {
	database, err := gorm.Open(DBDRIVER, "host="+DBHOST+" port="+DBPORT+" user="+DBUSER+" dbname="+DBNAME+" sslmode=disable password="+DBPASS)

	if err != nil {
		panic("Failed to connect to database!")
	}

	database.AutoMigrate(&Owner{})

	DB = database
}
