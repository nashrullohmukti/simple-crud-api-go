package models

import (
	"github.com/gofrs/uuid"
)

type Owner struct {
	ID          uuid.UUID `json:"id" gorm:"type:uuid;primary_key;default:uuid_generate_v4()"`
	Name        string    `json:"name"`
	Email       string    `json:"email"`
	PhoneNumber string    `json:"phone_number"`
	NpwpNumber  string    `json:"npwp_number"`
}

type CreateOwnerInput struct {
	Name        string `json:"name" binding:"required"`
	Email       string `json:"email" binding:"required"`
	PhoneNumber string `json:"phone_number" binding:"required"`
	NpwpNumber  string `json:"npwp_number" binding:"required"`
}

type UpdateOwnerInput struct {
	Name        string `json:"name"`
	Email       string `json:"email"`
	PhoneNumber string `json:"phone_number"`
	NpwpNumber  string `json:"npwp_number"`
}
