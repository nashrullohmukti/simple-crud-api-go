package app

import (
	"github.com/gin-gonic/gin"
	"gitlab.com/nashrullohmukti/simple-crud-api-go/app/controllers"
	"gitlab.com/nashrullohmukti/simple-crud-api-go/app/models"
)

func Routes() {
	r := gin.Default()
	models.ConnectDatabase()

	// Routes
	r.GET("/owners", controllers.FindOwners)
	r.POST("/owners", controllers.CreateOwner)
	r.GET("/owners/:id", controllers.FindOwner)
	r.PATCH("/owners/:id", controllers.UpdateOwner)
	r.DELETE("/owners/:id", controllers.DeleteOwner)

	r.Run()
}
